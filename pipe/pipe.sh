#!/usr/bin/env bash
#
# This pipe is an example to show how easy it is to create pipes for Bitbucket Pipelines.
#
# Required globals:
#   IMAGE_NAME
#   GCLOUD_API_KEY_BASE64
#

source "$(dirname "$0")/common.sh"

info "Executing the pipe..."

# Required parameters
IMAGE_NAME=${IMAGE_NAME:?'IMAGE_NAME variable missing.'}
GCLOUD_API_KEY_BASE64=${GCLOUD_API_KEY_BASE64:?'GCLOUD_API_KEY_BASE64 variable missing.'}

enable_debug

echo $GCLOUD_API_KEY_BASE64 | base64 -d > ~/.gcloud-gcr-api-key.json
run gcloud auth activate-service-account --key-file ~/.gcloud-gcr-api-key.json
run gcloud auth configure-docker --quiet
run docker build -t $IMAGE_NAME .
run docker push $IMAGE_NAME

if [[ "${status}" == "0" ]]; then
  success "Image was pushed to $IMAGE_NAME"
else
  fail "Error!"
fi
