FROM google/cloud-sdk:latest

RUN curl https://bitbucket.org/bitbucketpipelines/bitbucket-pipes-toolkit-bash/raw/0.4.0/common.sh > /common.sh

COPY pipe /
COPY LICENSE.txt pipe.yml /

ENTRYPOINT ["/pipe.sh"]
